package dev.dandroid.domain.usecase

import dev.dandroid.data.repo.SpaceXRepo
import dev.dandroid.domain.LaunchFilterStore
import dev.dandroid.domain.mapper.LaunchDetailsMapper
import dev.dandroid.domain.model.LaunchDetails
import javax.inject.Inject

class GetAllLaunchesUseCase @Inject constructor(
    private val spaceXRepo: SpaceXRepo,
    private val launchDetailsMapper: LaunchDetailsMapper
) {

    suspend fun getAllLaunches(launchFilters: LaunchFilterStore): Result<List<LaunchDetails>> =
        try {
            val year = launchFilters.year
            val missionSuccess = launchFilters.missionSuccess
            val sortBy = launchFilters.sortBy.parameter
            val orderBy = launchFilters.orderBy.parameter
            Result.Success(
                spaceXRepo.getLaunches(year, missionSuccess, sortBy, orderBy)
                    .map { launchDetailsMapper.map(it) })
        } catch (exception: Exception) {
            Result.Failure(exception)
        }
}