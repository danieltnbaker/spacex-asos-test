package dev.dandroid.domain.usecase

import dev.dandroid.data.repo.SpaceXRepo
import dev.dandroid.domain.mapper.CompanyInfoMapper
import dev.dandroid.domain.model.CompanyInfo
import javax.inject.Inject

class GetCompanyInfoUseCase @Inject constructor(
    private val spaceXRepo: SpaceXRepo,
    private val companyInfoMapper: CompanyInfoMapper
) {

    suspend fun getCompanyInfo(): Result<CompanyInfo> = try {
        Result.Success(companyInfoMapper.map(spaceXRepo.getCompanyInfo()))
    } catch (exception: Exception) {
        Result.Failure(exception)
    }
}