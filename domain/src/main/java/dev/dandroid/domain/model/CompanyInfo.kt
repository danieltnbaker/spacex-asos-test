package dev.dandroid.domain.model

data class CompanyInfo(
    val companyName: String,
    val founderName: String,
    val foundedYear: Int,
    val numberOfEmployees: Int,
    val numberOfLaunchSites: Int,
    val companyValuation: Long
)