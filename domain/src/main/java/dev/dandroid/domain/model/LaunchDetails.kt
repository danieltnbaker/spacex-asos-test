package dev.dandroid.domain.model

import java.util.*

data class LaunchDetails(
    val missionName: String,
    val missionYear: String,
    val missionDate: Date,
    val missionThumb: String,
    val missionSuccess: Boolean?,
    val rocket: RocketDetails
)
