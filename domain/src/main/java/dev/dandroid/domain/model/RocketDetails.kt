package dev.dandroid.domain.model

data class RocketDetails(
    val rocketName: String,
    val rocketType: String,
)
