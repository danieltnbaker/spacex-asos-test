package dev.dandroid.domain.mapper

import dev.dandroid.data.model.NetworkCompanyInfo
import dev.dandroid.domain.model.CompanyInfo
import javax.inject.Inject

class CompanyInfoMapper @Inject constructor(): Mapper<NetworkCompanyInfo, CompanyInfo> {

    override fun map(input: NetworkCompanyInfo): CompanyInfo = CompanyInfo(
        companyName = input.name,
        founderName = input.founder,
        foundedYear = input.founded,
        numberOfEmployees = input.employees,
        numberOfLaunchSites = input.launchSites,
        companyValuation = input.valuation
    )
}