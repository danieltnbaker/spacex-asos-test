package dev.dandroid.domain.mapper

import dev.dandroid.data.model.NetworkLaunchInfo
import dev.dandroid.domain.model.LaunchDetails
import dev.dandroid.domain.model.RocketDetails
import javax.inject.Inject

class LaunchDetailsMapper @Inject constructor(): Mapper<NetworkLaunchInfo, LaunchDetails> {

    override fun map(input: NetworkLaunchInfo): LaunchDetails = LaunchDetails(
        missionName = input.missionName,
        missionYear = input.launchYear,
        missionDate = input.launchDateUtc,
        missionThumb = input.links.missionPatchSmall ?: input.links.missionPatch ?: "",
        missionSuccess = input.launchSuccess,
        rocket = RocketDetails(
            rocketName = input.rocket.rocketName,
            rocketType = input.rocket.rocketType
        )
    )
}