package dev.dandroid.domain

import javax.inject.Inject

class LaunchFilterStore @Inject constructor() {
    var year: Int? = null
    var missionSuccess: Boolean? = null
    var sortBy: Sort = Sort.LAUNCH_YEAR
    var orderBy: Order = Order.DESC
}

enum class Sort(val parameter: String) {
    LAUNCH_YEAR("launch_date_utc"),
    FLIGHT_NUMBER("flight_number"),
    ROCKET_NAME("rocket_name")
}

enum class Order(val parameter: String) {
    ASC("asc"),
    DESC("desc")
}