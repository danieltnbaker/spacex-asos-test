package dev.dandroid.domain

import dev.dandroid.data.model.*
import java.util.*

object DomainModuleDataHelper {

    fun createDummyCompanyInfo(): NetworkCompanyInfo = NetworkCompanyInfo(
        name = "SpaceX",
        founder = "Elon Musk",
        founded = 2000,
        employees = 10000,
        vehicles = 5,
        launchSites = 2,
        testSites = 2,
        ceo = "Bowser",
        cto = "Mario",
        coo = "Luigi",
        ctoPropulsion = "",
        valuation = 1000000000,
        headquarters = NetworkCompanyHeadquarters(
            address = "1 first line",
            city = "Chicago",
            state = "Illinois"
        ),
        summary = "This company is the best",
    )

    fun createDummyLaunchInfo(): List<NetworkLaunchInfo> = listOf(
        NetworkLaunchInfo(
            flightNumber = 100,
            missionName = "Mission One",
            upcoming = true,
            launchYear = "2001",
            launchDateUtc = Date(),
            launchDateLocal = Date(),
            isTentative = false,
            tentativeMaxPrecision = "",
            tbd = false,
            launchWindow = 2,
            rocket = NetworkLaunchRocket("123", "Rocket One", "Rocket Type"),
            launchSuccess = true,
            links = NetworkLaunchLinks("mission patch", "mission patch thumb")
        ),
        NetworkLaunchInfo(
            flightNumber = 101,
            missionName = "Mission Two",
            upcoming = true,
            launchYear = "2002",
            launchDateUtc = Date(),
            launchDateLocal = Date(),
            isTentative = false,
            tentativeMaxPrecision = "",
            tbd = false,
            launchWindow = 2,
            rocket = NetworkLaunchRocket("123", "Rocket One", "Rocket Type"),
            launchSuccess = false,
            links = NetworkLaunchLinks("mission patch", null)
        )
    )
}