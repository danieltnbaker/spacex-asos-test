package dev.dandroid.domain.usecase

import dev.dandroid.data.repo.SpaceXRepo
import dev.dandroid.domain.DomainModuleDataHelper
import dev.dandroid.domain.mapper.CompanyInfoMapper
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

class GetCompanyInfoUseCaseTest {

    private val spaceXRepo = mockk<SpaceXRepo>()
    private val companyInfoMapper = CompanyInfoMapper()
    private val getCompanyInfoUseCase = GetCompanyInfoUseCase(spaceXRepo, companyInfoMapper)

    @Test
    fun getCompanyInfoSuccessTest() {
        val dummyCompanyInfo = DomainModuleDataHelper.createDummyCompanyInfo()
        coEvery { spaceXRepo.getCompanyInfo() } returns dummyCompanyInfo

        val info = runBlocking { getCompanyInfoUseCase.getCompanyInfo() } as Result.Success
        Assert.assertEquals(info.data.companyName, "SpaceX")
        Assert.assertEquals(info.data.numberOfEmployees, 10000)

        coVerify { spaceXRepo.getCompanyInfo() }
    }

    @Test(expected = Throwable::class)
    fun getCompanyInfoFailureTest() {
        coEvery { spaceXRepo.getCompanyInfo() } throws Throwable("this is a test")

        val info = runBlocking { getCompanyInfoUseCase.getCompanyInfo() } as Result.Failure
        Assert.assertEquals(info.throwable.message, "this is a test")

        coVerify { spaceXRepo.getCompanyInfo() }
    }
}