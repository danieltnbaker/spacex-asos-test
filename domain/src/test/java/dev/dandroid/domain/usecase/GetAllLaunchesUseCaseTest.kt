package dev.dandroid.domain.usecase

import dev.dandroid.data.repo.SpaceXRepo
import dev.dandroid.domain.DomainModuleDataHelper
import dev.dandroid.domain.LaunchFilterStore
import dev.dandroid.domain.mapper.LaunchDetailsMapper
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

class GetAllLaunchesUseCaseTest {

    private val spaceXRepo = mockk<SpaceXRepo>()
    private val launchDetailsMapper = LaunchDetailsMapper()
    private val getAllLaunchesUseCase = GetAllLaunchesUseCase(spaceXRepo, launchDetailsMapper)

    @Test
    fun getLaunchDetailsSuccessTest() {
        val dummyLaunchInfo = DomainModuleDataHelper.createDummyLaunchInfo()
        val sortBy = "launch_date_utc"
        val orderBy = "desc"

        coEvery { spaceXRepo.getLaunches(null, null, sortBy, orderBy) } returns dummyLaunchInfo

        val launchFilters = LaunchFilterStore()
        val info = runBlocking { getAllLaunchesUseCase.getAllLaunches(launchFilters) } as Result.Success
        Assert.assertEquals(info.data.first().missionName, "Mission One")
        Assert.assertEquals(info.data.first().missionSuccess, true)

        coVerify { getAllLaunchesUseCase.getAllLaunches(launchFilters) }
        coVerify { spaceXRepo.getLaunches(null, null, sortBy, orderBy) }
    }

    @Test(expected = Throwable::class)
    fun getLaunchDetailsFailureTest() {
        val sortBy = "launch_date_utc"
        val orderBy = "desc"
        coEvery { spaceXRepo.getLaunches(null, null, sortBy, orderBy) } throws Throwable("this is a test")

        val launchFilters = LaunchFilterStore()
        val info = runBlocking { getAllLaunchesUseCase.getAllLaunches(launchFilters) } as Result.Failure
        Assert.assertEquals(info.throwable.message, "this is a test")

        coVerify { spaceXRepo.getLaunches(null, null, sortBy, orderBy) }
    }
}