package dev.dandroid.domain.mapper

import dev.dandroid.domain.DomainModuleDataHelper
import org.junit.Assert
import org.junit.Test

class CompanyInfoMapperTest {

    @Test
    fun mappingSuccessTest() {
        val domainObject = CompanyInfoMapper().map(DomainModuleDataHelper.createDummyCompanyInfo())
        Assert.assertEquals(domainObject.companyName, "SpaceX")
        Assert.assertEquals(domainObject.founderName, "Elon Musk")
        Assert.assertEquals(domainObject.numberOfEmployees, 10000)
    }

    @Test
    fun mappingFailureTest() {
        val domainObject = CompanyInfoMapper().map(DomainModuleDataHelper.createDummyCompanyInfo())
        Assert.assertNotEquals(domainObject.companyName, "Nasa")
        Assert.assertNotEquals(domainObject.founderName, "Bob Geldof")
        Assert.assertNotEquals(domainObject.numberOfEmployees, 123)
    }
}