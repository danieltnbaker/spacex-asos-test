package dev.dandroid.domain.mapper

import dev.dandroid.domain.DomainModuleDataHelper
import org.junit.Assert
import org.junit.Test

class LaunchDetailsMapperTest {

    @Test
    fun mappingSuccessTest() {
        val domainObjects = DomainModuleDataHelper.createDummyLaunchInfo().map { LaunchDetailsMapper().map(it) }
        Assert.assertEquals(domainObjects.size, 2)
        Assert.assertEquals(domainObjects[0].missionThumb, "mission patch thumb")
        Assert.assertEquals(domainObjects[0].missionSuccess, true)
        Assert.assertEquals(domainObjects[1].missionSuccess, false)
    }

    @Test
    fun nullThumbRevertsToLargeImage() {
        val domainObjects = DomainModuleDataHelper.createDummyLaunchInfo().map { LaunchDetailsMapper().map(it) }
        Assert.assertEquals(domainObjects.size, 2)
        Assert.assertEquals(domainObjects[0].missionThumb, "mission patch thumb")
        Assert.assertEquals(domainObjects[1].missionThumb, "mission patch")
    }
}