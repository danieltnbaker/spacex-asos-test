package dev.dandroid.data

import dev.dandroid.data.network.SpaceXService
import dev.dandroid.data.repo.SpaceXRepo
import dev.dandroid.data.DataModuleDataHelper.createDummyCompanyInfo
import dev.dandroid.data.DataModuleDataHelper.createDummyLaunchInfo
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test

class SpaceXRepoTest {

    private val spaceXService: SpaceXService = mockk()
    private val spaceXRepo: SpaceXRepo = SpaceXRepo(spaceXService)

    @Test
    fun getCustomerInformationTest() {
        coEvery { spaceXService.getCompanyInformation() } returns createDummyCompanyInfo()

        val info = runBlocking { spaceXRepo.getCompanyInfo() }
        assertEquals(info.name, "SpaceX")
        assertEquals(info.cto, "Mario")

        coVerify { spaceXRepo.getCompanyInfo() }
        coVerify { spaceXService.getCompanyInformation() }
    }

    @Test
    fun getAllLaunchesTest() {
        val sortBy = "launch_date_utc"
        val orderBy = "desc"
        coEvery { spaceXService.getAllLaunches(null, null, sortBy, orderBy) } returns createDummyLaunchInfo()

        val launches = runBlocking { spaceXService.getAllLaunches(null, null, sortBy, orderBy) }
        assertEquals(launches[0].missionName, "Mission One")
        assertEquals(launches[1].missionName, "Mission Two")

        coVerify { spaceXRepo.getLaunches(null, null, sortBy, orderBy) }
        coVerify { spaceXService.getAllLaunches(null, null, sortBy, orderBy) }
    }
}