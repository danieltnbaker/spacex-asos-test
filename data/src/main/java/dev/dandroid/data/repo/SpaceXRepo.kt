package dev.dandroid.data.repo

import dev.dandroid.data.model.NetworkCompanyInfo
import dev.dandroid.data.model.NetworkLaunchInfo
import dev.dandroid.data.network.SpaceXService
import javax.inject.Inject

class SpaceXRepo @Inject constructor(private val service: SpaceXService) {

    suspend fun getCompanyInfo(): NetworkCompanyInfo = service.getCompanyInformation()

    suspend fun getLaunches(
        year: Int? = null,
        missionSuccess: Boolean? = null,
        sortBy: String,
        orderBy: String
    ): List<NetworkLaunchInfo> = service.getAllLaunches(year, missionSuccess, sortBy, orderBy)
}