package dev.dandroid.data.network

import dev.dandroid.data.model.NetworkCompanyInfo
import dev.dandroid.data.model.NetworkLaunchInfo
import retrofit2.http.GET
import retrofit2.http.Query

interface SpaceXService {

    @GET("info")
    suspend fun getCompanyInformation(): NetworkCompanyInfo

    @GET("launches")
    suspend fun getAllLaunches(
        @Query("launch_year") year: Int? = null,
        @Query("launch_success") missionSuccess: Boolean? = null,
        @Query("sort") sortBy: String,
        @Query("order") orderBy: String
    ): List<NetworkLaunchInfo>
}