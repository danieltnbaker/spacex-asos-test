package dev.dandroid.data.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class NetworkLaunchInfo(
    @SerializedName("flight_number") val flightNumber: Int,
    @SerializedName("mission_name") val missionName: String,
    val upcoming: Boolean,
    @SerializedName("launch_year") val launchYear: String,
    @SerializedName("launch_date_utc") val launchDateUtc: Date,
    @SerializedName("launch_date_local") val launchDateLocal: Date,
    @SerializedName("is_tentative") val isTentative: Boolean,
    @SerializedName("tentative_max_precision") val tentativeMaxPrecision: String,
    val tbd: Boolean,
    @SerializedName("launch_window") val launchWindow: Int,
    val rocket: NetworkLaunchRocket,
    @SerializedName("launch_success") val launchSuccess: Boolean?,
    val links: NetworkLaunchLinks
)

data class NetworkLaunchLinks(
    @SerializedName("mission_patch") val missionPatch: String?,
    @SerializedName("mission_patch_small") val missionPatchSmall: String?
)

data class NetworkLaunchRocket(
    @SerializedName("rocket_id") val rocketId: String,
    @SerializedName("rocket_name") val rocketName: String,
    @SerializedName("rocket_type") val rocketType: String
)