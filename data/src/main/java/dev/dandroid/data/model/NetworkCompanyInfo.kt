package dev.dandroid.data.model

import com.google.gson.annotations.SerializedName

data class NetworkCompanyInfo(
    val name: String,
    val founder: String,
    val founded: Int,
    val employees: Int,
    val vehicles: Int,
    @SerializedName("launch_sites") val launchSites: Int,
    @SerializedName("test_sites") val testSites: Int,
    val ceo: String,
    val cto: String,
    val coo: String,
    @SerializedName("cto_propulsion") val ctoPropulsion: String,
    val valuation: Long,
    val headquarters: NetworkCompanyHeadquarters,
    val summary: String
)

data class NetworkCompanyHeadquarters(
    val address: String,
    val city: String,
    val state: String
)