package dev.dandroid.spacex.ui.list

import android.os.Bundle
import android.view.*
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.chip.Chip
import dagger.hilt.android.AndroidEntryPoint
import dev.dandroid.domain.LaunchFilterStore
import dev.dandroid.domain.Order
import dev.dandroid.domain.Sort
import dev.dandroid.domain.model.CompanyInfo
import dev.dandroid.domain.model.LaunchDetails
import dev.dandroid.spacex.R
import dev.dandroid.spacex.databinding.FragmentLaunchListBinding
import java.text.NumberFormat
import java.util.*


@AndroidEntryPoint
class LaunchListFragment : Fragment() {

    private val viewModel: LaunchListViewModel by viewModels()

    private lateinit var binding: FragmentLaunchListBinding

    private var isManualUpdate: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLaunchListBinding.inflate(inflater, container, false)

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.loading.observe(viewLifecycleOwner, binding.swipeRefresh::setRefreshing)
        viewModel.companyInfo.observe(viewLifecycleOwner, ::setCompanyInformation)
        viewModel.launchDetails.observe(viewLifecycleOwner, ::setAdapterData)
        viewModel.filter.observe(viewLifecycleOwner, ::setSelectedFilters)

        binding.swipeRefresh.setOnRefreshListener { viewModel.refreshData() }
        binding.launchRecycler.layoutManager = LinearLayoutManager(requireContext())
        binding.launchRecycler.addItemDecoration(
            DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
        )

        binding.missionSuccessGroup.setOnCheckedChangeListener { _, checkedId ->
            if (!isManualUpdate) {
                viewModel.missionSuccessSelected(
                    when (checkedId) {
                        R.id.successfulYes -> true
                        R.id.successfulNo -> false
                        else -> null
                    }
                )
            }
        }
        binding.sortGroup.setOnCheckedChangeListener { _, checkedId ->
            if (!isManualUpdate) {
                viewModel.sortBySelected(
                    when (checkedId) {
                        R.id.sortFlightNumber -> Sort.FLIGHT_NUMBER
                        R.id.sortRocket -> Sort.ROCKET_NAME
                        else -> Sort.LAUNCH_YEAR
                    }
                )
            }
        }
        binding.orderGroup.setOnCheckedChangeListener { _, checkedId ->
            if (!isManualUpdate) {
                viewModel.orderBySelected(
                    when (checkedId) {
                        R.id.orderAsc -> Order.ASC
                        else -> Order.DESC
                    }
                )
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_filter, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.filter) {
            showFilterMenu()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showFilterMenu() {
        if (binding.drawerLayout.isDrawerOpen(binding.filterDrawer)) {
            binding.drawerLayout.closeDrawer(binding.filterDrawer)
        } else {
            binding.drawerLayout.openDrawer(binding.filterDrawer)
        }
    }

    private fun setCompanyInformation(companyInfo: CompanyInfo) {
        binding.companyInformation.text = getString(
            R.string.company_information_format,
            companyInfo.companyName,
            companyInfo.founderName,
            companyInfo.foundedYear,
            companyInfo.numberOfEmployees,
            companyInfo.numberOfLaunchSites,
            NumberFormat.getNumberInstance(Locale.US).format(companyInfo.companyValuation)
        )
    }

    private fun setAdapterData(launchList: List<LaunchDetails>) {
        binding.emptyView.isVisible = launchList.isEmpty()
        binding.launchRecycler.isVisible = launchList.isNotEmpty()
        binding.launchRecycler.adapter = LaunchListAdapter(launchList)
        setYearChips(launchList.map { it.missionYear }.distinct())
    }

    private fun setYearChips(yearList: List<String>) {
        binding.missionYearChips.setOnCheckedChangeListener(null)
        binding.missionYearChips.removeAllViews()
        yearList.forEach {
            val chip = createChip(it)
            if (yearList.size == 1) {
                chip.isChecked = true
            }
            binding.missionYearChips.addView(chip)
        }
        binding.missionYearChips.setOnCheckedChangeListener { group, checkedId ->
            if (!isManualUpdate) {
                val checkedChip = group.findViewById<Chip>(checkedId)
                val year = if (checkedChip?.text.isNullOrEmpty()) null
                else checkedChip?.text.toString()
                viewModel.missionYearSelected(year)
            }
        }
    }

    private fun setSelectedFilters(filterStore: LaunchFilterStore) {
        isManualUpdate = true
        filterStore.year?.let { year ->
            var chip = binding.missionYearChips.findViewWithTag<Chip>(year.toString())
            if (chip == null) {
                chip = createChip(year.toString())
                binding.missionYearChips.addView(chip)
            }
            chip.isChecked = true
        }
        when (filterStore.missionSuccess) {
            true -> binding.successfulYes.isChecked = true
            false -> binding.successfulNo.isChecked = true
            else -> binding.successfulAll.isChecked = true
        }
        when (filterStore.sortBy) {
            Sort.ROCKET_NAME -> binding.sortRocket.isChecked = true
            Sort.FLIGHT_NUMBER -> binding.sortFlightNumber.isChecked = true
            else -> binding.sortYear.isChecked = true
        }
        when (filterStore.orderBy) {
            Order.ASC -> binding.orderAsc.isChecked = true
            else -> binding.orderDesc.isChecked = true
        }
        isManualUpdate = false
    }

    private fun createChip(year: String): Chip {
        val chip = Chip(requireContext())
        chip.text = year
        chip.tag = year
        return chip
    }
}