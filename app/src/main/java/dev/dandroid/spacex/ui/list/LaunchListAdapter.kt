package dev.dandroid.spacex.ui.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import dev.dandroid.core.getDateTimeString
import dev.dandroid.core.getDaysSinceLaunch
import dev.dandroid.core.isPastDate
import dev.dandroid.domain.model.LaunchDetails
import dev.dandroid.spacex.R
import dev.dandroid.spacex.databinding.ListItemLaunchBinding
import java.util.*

class LaunchListAdapter(
    private val data: List<LaunchDetails>
) : RecyclerView.Adapter<LaunchListAdapter.LaunchViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LaunchViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ListItemLaunchBinding.inflate(inflater, parent, false)
        return LaunchViewHolder(binding)
    }

    override fun onBindViewHolder(holder: LaunchViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size

    inner class LaunchViewHolder(
        private val binding: ListItemLaunchBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(launchDetails: LaunchDetails) {
            Glide.with(itemView.context)
                .load(launchDetails.missionThumb)
                .placeholder(R.drawable.ic_nasa_grey)
                .into(binding.missionPatch)
            binding.successImage.setImageResource(when (launchDetails.missionSuccess) {
                true -> R.drawable.ic_success
                false -> R.drawable.ic_failure
                else -> R.drawable.ic_unknown
            })
            binding.missionName.text = launchDetails.missionName
            binding.missionDate.text = launchDetails.missionDate.getDateTimeString()
            binding.missionRocket.text = String.format(
                Locale.getDefault(),
                "%s / %s",
                launchDetails.rocket.rocketName,
                launchDetails.rocket.rocketType
            )
            if (launchDetails.missionDate.isPastDate()) {
                binding.missionCountdownLabel.setText(R.string.launch_list_mission_countdown_since_label)
            } else {
                binding.missionCountdownLabel.setText(R.string.launch_list_mission_countdown_from_label)
            }
            binding.missionCountdown.text = launchDetails.missionDate.getDaysSinceLaunch()
        }
    }
}