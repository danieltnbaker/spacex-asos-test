package dev.dandroid.spacex.ui.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dev.dandroid.domain.LaunchFilterStore
import dev.dandroid.domain.Order
import dev.dandroid.domain.Sort
import dev.dandroid.domain.model.CompanyInfo
import dev.dandroid.domain.model.LaunchDetails
import dev.dandroid.domain.usecase.GetAllLaunchesUseCase
import dev.dandroid.domain.usecase.GetCompanyInfoUseCase
import dev.dandroid.domain.usecase.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LaunchListViewModel @Inject constructor(
    private val launchFilterStore: LaunchFilterStore,
    private val getAllLaunchesUseCase: GetAllLaunchesUseCase,
    private val getCompanyInfoUseCase: GetCompanyInfoUseCase
) : ViewModel() {

    val loading: MutableLiveData<Boolean> by lazy { MutableLiveData<Boolean>() }
    val companyInfo: MutableLiveData<CompanyInfo> by lazy { MutableLiveData<CompanyInfo>() }
    val launchDetails: MutableLiveData<List<LaunchDetails>> by lazy { MutableLiveData<List<LaunchDetails>>() }
    val filter: MutableLiveData<LaunchFilterStore> by lazy { MutableLiveData<LaunchFilterStore>() }

    init {
        loadData()
    }

    fun refreshData() {
        if (loading.value != true) loadData()
    }

    private fun loadData() {
        loading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {
            when (val result = getCompanyInfoUseCase.getCompanyInfo()) {
                is Result.Success -> companyInfo.postValue(result.data)
                is Result.Failure -> result.throwable.printStackTrace()
            }
            when (val result = getAllLaunchesUseCase.getAllLaunches(launchFilterStore)) {
                is Result.Success -> launchDetails.postValue(result.data)
                is Result.Failure -> result.throwable.printStackTrace()
            }
            loading.postValue(false)
        }
    }

    fun missionYearSelected(year: String?) {
        val selectedYear = if (year.isNullOrEmpty()) null else year.toInt()
        launchFilterStore.year = if (launchFilterStore.year == selectedYear) null else selectedYear
        filterLaunchResults()
    }

    fun missionSuccessSelected(success: Boolean?) {
        launchFilterStore.missionSuccess = success
        filterLaunchResults()
    }

    fun sortBySelected(sort: Sort) {
        launchFilterStore.sortBy = sort
        filterLaunchResults()
    }

    fun orderBySelected(order: Order) {
        launchFilterStore.orderBy = order
        filterLaunchResults()
    }

    private fun filterLaunchResults() {
        loading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {
            when (val result = getAllLaunchesUseCase.getAllLaunches(launchFilterStore)) {
                is Result.Success -> {
                    launchDetails.postValue(result.data)
                    filter.postValue(launchFilterStore)
                }
                is Result.Failure -> result.throwable.printStackTrace()
            }
            loading.postValue(false)
        }
    }
}