package dev.dandroid.core

import org.threeten.bp.Instant
import org.threeten.bp.ZoneId
import org.threeten.bp.ZoneOffset
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.abs

fun Date.getDateTimeString(): String {
    val format = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
    return format.format(this)
}

fun Date.isPastDate(): Boolean {
    val zoneId = ZoneId.ofOffset("UTC", ZoneOffset.ofHours(0))
    val todayDate = Instant.ofEpochMilli(Date().time).atZone(zoneId).toLocalDate()
    val launchDate = Instant.ofEpochMilli(time).atZone(zoneId).toLocalDate()
    return launchDate.year < todayDate.year
            || (launchDate.year == todayDate.year && launchDate.dayOfYear < todayDate.dayOfYear)
}

fun Date.getDaysSinceLaunch(): String {
    val diffInMillis = if (isPastDate()) abs(Date().time - time) else abs(time - Date().time)
    return TimeUnit.DAYS.convert(diffInMillis, TimeUnit.MILLISECONDS).toString()
}